package backintimedebug;

import com.sun.jdi.*;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.IllegalConnectorArgumentsException;
import com.sun.jdi.connect.LaunchingConnector;
import com.sun.jdi.connect.VMStartException;
import com.sun.jdi.event.*;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.jdi.request.ClassPrepareRequest;
import com.sun.jdi.request.MethodEntryRequest;
import com.sun.jdi.request.StepRequest;
import trace.ProgramTrace;
import trace.Trace;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;


public class Debugger {
    public static final String CLASSNAME = Test.class.getName();
    public static int STARTING_LINE = 8;
    public static int c = 0;

    public static void main(String[] args) {
        LaunchingConnector launchingConnector = Bootstrap.virtualMachineManager().defaultConnector();
        Map<String, Connector.Argument> argumentMap = launchingConnector.defaultArguments();
        argumentMap.get("main").setValue(CLASSNAME);
        VirtualMachine vm = null;
        try {
            vm = launchingConnector.launch(argumentMap);
            ClassPrepareRequest classPrepareRequest = vm.eventRequestManager().createClassPrepareRequest();
            classPrepareRequest.addClassFilter(CLASSNAME);
            classPrepareRequest.enable();

            EventSet eventSet = null;
            boolean isVMDead = false;
            while (!isVMDead && (eventSet = vm.eventQueue().remove()) != null) {
                for (Event event : eventSet) {
                    //System.out.println(event);
                    if (event instanceof ClassPrepareEvent) {
                        List<ReferenceType> listRef = vm.classesByName(CLASSNAME);
                        if (listRef.size()>0){
                            ReferenceType testReferenceType =listRef.get(0);
                            Location location = testReferenceType.locationsOfLine(STARTING_LINE).get(0);
                            BreakpointRequest breakpointRequest = vm.eventRequestManager().createBreakpointRequest(location);
                            breakpointRequest.enable();



                    }

                    if (event instanceof MethodEntryEvent) {
                        MethodEntryEvent methodEntryEvent = (MethodEntryEvent) event;
                        System.out.println("eeeeeeeeeeeeeeeeee" + methodEntryEvent.method().name());
                    }

                    }
                    if (event instanceof BreakpointEvent) {
                        StepRequest stepRequest = vm.eventRequestManager().createStepRequest(((BreakpointEvent) event).thread(), StepRequest.STEP_MIN , StepRequest.STEP_INTO);
                        stepRequest.enable();
                        //MethodEntryRequest methodEntryRequest = vm.eventRequestManager().createMethodEntryRequest();
                        //methodEntryRequest.enable();
                    }

                    if (event instanceof StepEvent) {
                        StepEvent stepEvent = (StepEvent) event;
                        Location sLoc = stepEvent.location();
                        if (sLoc.method().name().equals("exit")){
                            vm.exit(1);
                            isVMDead = true;
                            break;
                        }
                        c++;
                        System.out.println(c + " : " + sLoc.method().name());
                        System.out.println(stepEvent.thread().status());
                        if (!stepEvent.thread().isSuspended()){
                            stepEvent.thread().suspend();
                            System.out.println("suspend");
                        }
                        System.out.println(stepEvent.thread().status());

                        if (stepEvent.thread().frameCount()>0) {
                            StackFrame sf = stepEvent.thread().frame(0);
                            List<LocalVariable> listVar = sf.visibleVariables();
                            for (LocalVariable locVar : listVar) {
                                new Trace(sLoc.lineNumber(), sLoc.method().name(), locVar.name(), sf.getValue(locVar).toString(), sLoc.declaringType().name(), sLoc.sourcePath());
                            }
                        }
                    }

                    if (event instanceof VMDisconnectEvent) {
                        isVMDead = true;
                        break;
                    }
                    vm.resume();
                }
            }


        } catch (IllegalConnectorArgumentsException | VMStartException | IOException | InterruptedException | AbsentInformationException | IncompatibleThreadStateException e) {
            e.printStackTrace();
        } finally {
            InputStream ip = vm.process().getInputStream();
            StringBuilder sb = new StringBuilder();
            try {
                for (int ch; (ch = ip.read()) != -1; ) {
                    sb.append((char) ch);
                }
                if (sb.length()>0) {
                    System.out.println("Program out : \n" + sb);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            ProgramTrace.getInstance().logTrace();
            ProgramTrace.getInstance().saveTrace();
        }

    }
}
