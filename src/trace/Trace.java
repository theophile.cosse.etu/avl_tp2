package trace;

import java.io.Serializable;

public class Trace implements Serializable {
    private static final long serialVersionUID = 1L;

    private long time;
    private String methodName = null;
    private String variableName = null;
    private String variableValue = null;
    private String className = null;
    private String fileName = null;
    private int lineNumber = -1;

    public Trace(int lineNumber,String methodName, String variableName, String variableValue, String className, String fileName) {
        this.methodName = methodName;
        this.variableName = variableName;
        this.variableValue = variableValue;
        this.time = System.nanoTime();
        this.className = className;
        this.fileName = fileName;
        this.lineNumber = lineNumber;
        ProgramTrace.getInstance().addTrace(this);
    }

    @Override
    public String toString() {
        String result = "t:" + time;
        if (methodName!=null)
            result+=" mn : " + methodName;
        if (variableName!=null)
            result+=" var : " + variableName;
        if (variableValue!=null)
            result+=" val : " + variableValue;
        if (className!=null)
            result+=" cn : " + className;
        if (fileName!=null)
            result+=" fn : " + fileName;
        if (lineNumber!=-1)
            result+=" ln : " + lineNumber;
        return result;
    }
}
