package trace;

public class Test {
    public static void main(String[] args) {
        //test1();
        test2();
    }

    public static void test1(){
        //new Trace("methodCool","v1",3);
        //new Trace("methodCool","v1",6);
        //new Trace("methodPasCool");
        //new Trace();

        ProgramTrace.getInstance().logTrace();
        ProgramTrace.getInstance().saveTrace();
    }
    public static void test2(){
        ProgramTrace pt =  ProgramTrace.getInstance();
        pt.loadTrace("program_trace.trace");
        Trace currTrace;
        while((currTrace=pt.nextTrace())!=null){
            System.out.println(currTrace);
        }
    }
}
