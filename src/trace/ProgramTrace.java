package trace;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ProgramTrace {
    private static ProgramTrace instance;

    private ArrayList<Trace> traceArrayList = new ArrayList<Trace>();
    private final String LOG_FILE_PATH = "./";
    private final String OBJECTS_FILE_PATH = "./";
    private ArrayList<String> methodCallStack = new ArrayList<String>();

    private int currentTraceIndex = 0;

    private ProgramTrace() {
    }

    public static ProgramTrace getInstance() {
        if (instance == null) {
            instance = new ProgramTrace();
        }
        return instance;
    }

    public void addTrace(Trace trace) {
        traceArrayList.add(trace);
    }

    public Trace nextTrace() {
        Trace result = null;
        if (currentTraceIndex < traceArrayList.size()) {
            result = traceArrayList.get(currentTraceIndex);
            currentTraceIndex++;
        }
        return result;
    }

    public Trace PreviousTrace() {
        Trace result = null;
        if (currentTraceIndex >= 0) {
            result = traceArrayList.get(currentTraceIndex);
            currentTraceIndex--;
        }
        return result;
    }

    public void saveTrace() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-hh-mm");
        Date date = new Date();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(OBJECTS_FILE_PATH + "program_trace.trace"));
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(traceArrayList);

            objectOutputStream.close();
            fileOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadTrace(String filename) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(filename));
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            traceArrayList = (ArrayList<Trace>) objectInputStream.readObject();

            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void logTrace() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-hh-mm");
        Date date = new Date();
        try {
            PrintWriter writer = new PrintWriter(LOG_FILE_PATH + dateFormat.format(date) + "log.log", "UTF-8");
            for (Trace trace : traceArrayList) {
                writer.println(trace);
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void addToStack(String method) {
        methodCallStack.add(method);
    }

    public ArrayList<String> getMethodCallStack() {
        return methodCallStack;
    }
}

